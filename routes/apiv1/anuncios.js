/**
 * Created by begohorma on 19/10/16.
 */
'use strict';

var express = require('express');
var router = express.Router();

var mongoose=require('mongoose');
var Anuncio = mongoose.model('Anuncio');

//autenticación
var jwtAuth = require('../../lib/jwtAuth');

//configuración local
var localConfig =require('../../localConfig');

//CustomError
var CustomError=require('./CustomError');

// Comprobar que se está pasando el token de usuario registrado
router.use(jwtAuth());

//lista de anuncios con filtros
router.get('/',function (req,res,next) {

    //recoger parametros del request
    

    //filtros
    let venta =req.query.venta;
    let tags=req.query.tags;
    let precio=req.query.precio;
    let nombre= req.query.nombre;

    //modificadores

    //idioma
    let idioma= req.query.lg;

    //Paginación: El contenido de las páginas viene delimitado por los valores de start y limit
    //Valor a partir del cual se comienzan a mostrar los datos
    //Por defecto es 0. Se cominenza a mostrar el primer resultado de la búsqueda
    let start = parseInt(req.query.start) || 0;

    //valor de elementos mostrados.
    let limit= parseInt(req.query.limit) || 0;

    //incluir totales - Se mostrará el total de documentos que cumplen los criterios de búsqueda
    let incluirTotal=req.query.includeTotal || false;

    //campo por el que se realiza la ordenación de los resultados
    let sort = req.query.sort || null;

    //seleccion de campos a mostrar
    let fields= req.query.fields ||null;

    //Provocar error para pruebas
    //limit='abc';

    //inicialización filter
    let filter={};

    //si venta tiene valor asignar al filtro su valor

    if(typeof idioma !== 'undefined'){
        if(idioma ==='en' || idioma ==='es'){
            localConfig.language.userLanguage=idioma;
        }
        else{
            return(next(new CustomError('LANGUAGE_NOT_AVAILABLE')));
        }

    }

    if(typeof venta !== 'undefined'){
        if (venta !== 'true' && venta !== 'false')
        {
            return(next(new CustomError('WRONG_VENTA_VALUE')));
        }
        else
        {
            filter.venta=venta;
        }
    }

    //si tags tiene un valor asignar al filtro su valor
    //obtendrá los anunciós que en el array de tags tengan alguno de los tags indicados

    if(typeof tags !== 'undefined'){
        console.log('typeof (tags):', typeof(tags));
        if (typeof tags ==='string'){
            filter.tags=tags;
        }
        else
        {
            filter.tags ={$in:tags};
        }

    }

    //Si precio tiene valor asignar el filtro en función de su valor
    //para cualquier valor de precio que siga la estructura [preciomin]-[preciomax]
    if(typeof precio !== 'undefined'){
        let p=precio.split('-');
        let pMin=p[0];
        let pMax=p[1];

        console.log('p',p);
        console.log('pMin',pMin);
        console.log('pMax',pMax);

        if(typeof(pMax)==='undefined'){
            //si no hay - a pMax no se le asignará nada
            //anuncios con precio = precio pasado
            console.log('= precio',precio);

            if(!isNaN(parseFloat(precio))){
                filter.precio=precio;
            }
            else{
                return(next(new CustomError('WRONG_PRECIO_VALUE')));
            }

        }
        else{
            if(pMin!=='' && pMax !==''){
                //anuncios con precio >=pMin y precio <=PMax
                console.log('entre pMin y pMax');

               if(!isNaN(parseFloat(pMin)) && !isNaN(parseFloat(pMax)))
                    {
                        filter.precio={'$gte':pMin,'$lte':pMax};
                    }
                else
                {
                    return(next(new CustomError('WRONG_P_MIN_OR_P_MAX_VALUE')));
                }

            }
            else{
                if(pMax===''){
                    //no hay pMax
                    //anuncios con precio >=pMin
                    console.log('>= pMin');

                   if(!isNaN(parseFloat(pMin))){
                        filter.precio={'$gte':pMin};
                    }
                    else{
                        return(next(new CustomError('WRONG_P_MIN_VALUE')));
                    }

                }
                else{
                    if(pMin===''){
                        //no hay pMin
                        //anuncios con precio <=pMax
                        console.log('<= pMax');

                        if(!isNaN(parseFloat(pMax))){
                            filter.precio={'$lte':pMax};
                        }
                        else{
                            return(next(new CustomError('WRONG_P_MAX_VALUE')));
                        }

                    }
                }
            }
        }

        //Para combinación fija de valores de entrada
        // switch (precio){
        //     case '10-50':
        //         //anuncios con precio >=10 y precio <=50
        //         filter.precio={'$gte':'10','$lte':'50'};
        //         break;
        //     case '10-' :
        //         //anuncios con precio >=10
        //         filter.precio={'$gte':'10'};
        //         break;
        //     case '-50' :
        //         //anuncios con precio <=50
        //         filter.precio={'$lte':'50'};
        //         break;
        //     case '50':
        //         //anuncios con precio =50
        //         filter.precio=precio;
        //         break;
        // }

    }

    //Si nombre tiene un valor utilizar expresión regular para buscar
    //lon nombre que empiezen por el valor pasado como parámetro
    //pattern tiene la expresión regular
    //option i:case-sensitive-match. **PREGUNTAR - ¿Funciona la i?**
    if(typeof nombre !== 'undefined'){
        // var pattern= '^'+ nombre;
        // filter.nombre= {$regex:pattern,$options:'i'};
        filter.nombre= new RegExp('^'+nombre,'i');
    }


    console.log('incluirTotal',incluirTotal);


    if(typeof incluirTotal !== 'undefined'){
        if (incluirTotal !== 'true' && incluirTotal !== false){
            return(next(new CustomError('WRONG_INCLUDE_TOTAL_VALUE')));
        }
    }



    if(incluirTotal)
    {
        //Con totales
         //TODO - cambiar por promesas

            //Obtener el total de anuncios posibles

            Anuncio.totalResults(filter,function(err,totalAnuncios){
                if(err){
                    console.log('Error obteniendo total anuncios');
                    return(next(new CustomError('TOTAL_ADVERTISEMENTS',err.message,err.code)));
                }

                //hacer consulta
                Anuncio.list(filter,start,limit,sort,fields,function(err,anuncios){
                    if(err){
                        return next( new CustomError('LISTING_ADVERTISEMENTS',err.message,err.code));
                    }

                    res.json({success:true, anuncios:anuncios,totalResultados:totalAnuncios});
                });
            });
    }
    else
    {
        //TODO - Este bloque se usa dos veces. habría que refactorizarlo. ¿Se soluciona con promesas?
        //hacer consulta
        Anuncio.list(filter,start,limit,sort,fields,function(err,anuncios){
            if(err){
                return next( new CustomError('LISTING_ADVERTISEMENTS',err.message,err.code));
            }

            res.json({success:true, auncios:anuncios});
        });
    }

});


//Crear anuncio de prueba

router.post('/',function (req,res,next) {
   var anuncio = new Anuncio(req.body);
    anuncio.save(function (err,anuncioGuardado) {
       if(err){
           next( new CustomError('ADDING_ADVERTISEMENTS',err.message,err.code));
           return;
       }
       res.json({success: true, anuncio: anuncioGuardado});
    });
});

module.exports =router;





