/**
 * Created by begohorma on 24/10/16.
 */
'use strict';

var traducir= require('../../lib/traducir');
var localConfig =require('../../localConfig');

var CustomError= function(clave,descripcion,codigo){
    this.name='Nodepop 1.0  Error';
    this.mensaje=traducir(clave,localConfig.language.userLanguage);
    this.descripcion= descripcion || '';
    this.codigo=codigo || '';
};



module.exports= CustomError;