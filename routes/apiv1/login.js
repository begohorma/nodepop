/**
 * Created by begohorma on 25/10/16.
 */
'use strict';

var express = require('express');
var router = express.Router();

//autenticación
var jwt = require('jsonwebtoken');
//configuración local
var localConfig =require('../../localConfig');

require('../../models/Usuario');
var mongoose= require('mongoose');
var Usuario = mongoose.model('Usuario');

//hash para la clave
var hash=require('hash.js');

//CustomError
var CustomError=require('./CustomError');

router.post('/', function (req,res,next){

    let nombre = req.body.nombre;
    let email=req.body.email;
    let clave= req.body.clave;

    //Comprobar que se han pasado todos los parámetros
    if(nombre === '' || typeof nombre === 'undefined'){
        return next(new CustomError('USER_NAME_REQUIRED'));
    }

    if(email === '' || typeof email === 'undefined'){
        return next(new CustomError('MAIL_REQUIRED'));
    }
    if(clave === '' || typeof clave === 'undefined'){
        return next(new CustomError('CLAVE_REQUIRED'));
    }


    //buscar usuario en la BD
    Usuario.findByEmail(email, function(err, usuarioEncontrado) {
        if (err) {
            return (next(new CustomError('SHEARCHING_USER_BY_EMAIL',err.message,err.code)));
        }
        console.log('usuarioEncontrado',usuarioEncontrado);
        if(usuarioEncontrado.length === 0){
            return (next(new CustomError('USER_NOT_FOUND')));
        }

       // Si hay un usuario con ese email hay que verificar la contraseña
        //aplicar hash a la clave

        let clavehash=hash.sha256().update(clave).digest('hex');

        if (usuarioEncontrado[0].clave !== clavehash ){
            return (next(new CustomError('WRONG_PASS')));
        }
        else{
            //el usuario existe y la clave es correcta
            //crear el token y devolverlo

            let token = jwt.sign({id:usuarioEncontrado.id},localConfig.jwt.secret,{expiresIn:localConfig.jwt.expiresIn});

            res.json({sucess:true, token:token});
        }
    });

});




module.exports = router;