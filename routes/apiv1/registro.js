/**
 * Created by begohorma on 23/10/16.
 */
'use strict';
var express = require('express');
var router = express.Router();

var mongoose=require('mongoose');
var Usuario = mongoose.model('Usuario');

//CustomError
var CustomError=require('./CustomError');

//hash para la clave
var hash=require('hash.js');

router.post('/',function (req,res,next) {

    //recoger los distintos parámtros del body para obtener la clave y guardarla en un hash
    let nombre= req.body.nombre;
    let email = req.body.email;
    let clave = req.body.clave;


    //Comprobar que se han pasado todos los parámetros
    if(nombre === '' || typeof nombre === 'undefined'){
        return next(new CustomError('USER_NAME_REQUIRED'));
    }
    //todo incluir validación de estructura del email con expresion regular

    if(email === '' || typeof email === 'undefined'){
        return next(new CustomError('MAIL_REQUIRED'));
    }
    if(clave === '' || typeof clave === 'undefined'){
        return next(new CustomError('CLAVE_REQUIRED'));
    }

    //comprobar que el email no existe ya enla bd.

    Usuario.findByEmail(email, function(err, usuarioEncontrado) {
        if (err) {
            return (next(new CustomError('SHEARCHING_USER_BY_EMAIL',err.message,err.code)));
        }

        if(usuarioEncontrado.length >0){
            return (next(new CustomError('EMAIL_ALREADY_EXISTS')));
        }

        //Si esta ok
        //aplicar hash a la clave
        let clavehash=hash.sha256().update(clave).digest('hex');


        let usuario = new Usuario({nombre:nombre,email:email,clave:clavehash});

        usuario.save(function (err,usuarioGuardado) {
            if(err){
                next( new CustomError('ADDING_USER',err.message,err.code));
                return;
            }
            res.json({success: true, usuario: {nombre:usuarioGuardado.nombre, email:usuarioGuardado.email, clave:usuarioGuardado.clave}});
        });
    });

});

module.exports =router;