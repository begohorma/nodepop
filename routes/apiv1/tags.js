/**
 * Created by begohorma on 24/10/16.
 */
'use strict';
var express = require('express');
var router = express.Router();

//autenticación
var jwtAuth = require('../../lib/jwtAuth');

var tags= require('./tags.json');

// Comprobar que se está pasando el token de usuario registrado
router.use(jwtAuth());

router.get('/', function (req,res) {
   res.json({success:true,tags:tags.tags});
});
module.exports =router;