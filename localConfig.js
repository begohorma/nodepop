/**
 * Created by begohorma on 28/10/16.
 */
'use strict';
module.exports ={
  jwt: {
      secret:'S3cr3tN0d3P0PAp1S7r1ngBH',
      expiresIn: '2days'
  },
  testUser:{
    name:'testUser',
    email:'testemail@gmail.com',
    clave: '73s7Key'
  },
  language:{
      availableLanguages:['en','es'],
      userLanguage:'en' //default
  }

};