'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

//Conexión a la BD
require('./lib/mongoConnection');

//Cargar modelos
require('./models/Anuncio');
require('./models/Usuario');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

//rutas
app.use('/apiv1', require('./routes/index'));
app.use('/apiv1/login',require('./routes/apiv1/login'));
app.use('/apiv1/registro',require('./routes/apiv1/registro'));
//rutas con autenticación requerida
app.use('/apiv1/anuncios',require('./routes/apiv1/anuncios'));
app.use('/apiv1/tags',require('./routes/apiv1/tags'));




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    // si es una petición de API devolvemos JSON, sino una página
    if (isAPI(req)) {
      res.json({success: false, error: err});
    } else {
      res.render('error', {
        message: err.message,
        error: err
      });
    }

  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  // si es una petición de API devolvemos JSON, sino una página
  if (isAPI(req)) {
    res.json({success: false, error: err});
  } else {
    res.render('error', {
      message: err.message,
      error: err
    });
  }
});

function isAPI(req) {
  return req.originalUrl.indexOf('/api') === 0;
}

module.exports = app;
