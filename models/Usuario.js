/**
 * Created by begohorma on 19/10/16.
 */
'use strict';


var mongoose= require('mongoose');

//definir esquema de los usuarios

var usuarioSchema = mongoose.Schema({
    nombre:{
       type: String,
       required:true
    },
    email:{
        type:String,
        required:true,
        index:true,
        unique:true
    },
    clave: {
        type:String,
        required:true
    }
});

//métodos del esquema usuario

// metodo de clase para borrrar todos los anuncios
usuarioSchema.statics.deleteAll= function (cb) {
    //Elimina todos los registros (no se pasa filtro).
    Usuario.remove({}, function (err) {
        if(err)
        {
            return cb(err);
        }
        cb(null);
    });
};

//buscar usuario por email
usuarioSchema.statics.findByEmail=function(email,cb){
    Usuario.find({email:email})
        .exec(function(err, usuario){
            if(err)
            {
                return cb(err);
            }
           cb(null,usuario);
        });
};


//Asociar el esquema al modelo
var Usuario = mongoose.model('Usuario',usuarioSchema); //Hoisting
