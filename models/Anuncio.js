/**
 * Created by begohorma on 19/10/16.
 */
'use strict';

var mongoose= require('mongoose');

//definir esquema de los anuncios

var anuncioSchema = mongoose.Schema({
    nombre: {
       type: String,
        required:true,
        index:true
    },
    venta:{
        type:Boolean,
        index:true
    },
    precio:{
      type:Number,
        index:true
    },
    foto: String,
    tags:{
        type : [String],
        index:true
    }
});

//métodos del esquema anuncio

// metodo de clase para borrrar todos los anuncios
anuncioSchema.statics.deleteAll= function (cb) {
  //Elimina todos los registros (no se pasa filtro).
    Anuncio.remove({}, function (err) {
       if(err)
       {
           return cb(err);
       }
       cb(null);
    });
};

//listar con filtros

anuncioSchema.statics.list= function(filter,start,limit,sort,fields,cb){
    let query = Anuncio.find(filter);
    query.skip(start);
    query.limit(limit);
    query.sort(sort);
    query.select(fields);
    query.exec(function (err,anuncios) {
       if(err){
           return cb(err);
       }
       cb(null,anuncios);
    });
};

anuncioSchema.statics.totalResults=function(filter,cb){
    let query =Anuncio.find(filter);
    query.count();
    query.exec(function(err,total){
        if(err){
            return cb(err);
        }
        cb(null,total);
    });
};

//Asociar el esquema al modelo. 
var Anuncio = mongoose.model('Anuncio',anuncioSchema); //Hoisting
 



