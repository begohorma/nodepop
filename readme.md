# Nodepop 1.0
---

##Introducción
Bienvenido a Nodepop, una API de venta de artículos de segunda mano.

Esta documentación te permitirá familiarizarte con las operaciones disponibles y cómo consumirlas con peticiones HTTP

El API intenta aprovechar los recursos del servidor por lo que iniciará tantos workers como núcleos disponga el servidor.


## Requisitos
Es necesario tener instalados:

- node.js

- mongoDB

Para realizar peticiones de tipo **POST** puede utilizar la herramienta postman: https://www.getpostman.com

  - En el body seleccionar la opción: **x-www-form-urlencoded**

## Depliegue en Servidor

La API Nodepop está desplegada en un servidor AWS. La url de acceso es: **[http://begohorma.com](http://begohorma.com)** o **[http://www.begohorma.com](http://www.begohorma.com)** 

Las peticiones HTTP se redirigen autimáticamente a peticiones HTTPS

### Utilizar las operaciones del API desde el servidor
Para utilizar las operaciones del API se pueden seguir las instrucciones indicadas en esta documentación para su uso desde local en el apartado: **Operaciones del API** pero se debe **modificar las urls de acceso** sustituyendo **`http://localhost:3000`** por **`http://begohorma.com`** 

El servidor utiliza nginx como proxy inverso por lo que no es necesario indicar el puerto en las peticiones.

Las peticiones POST para el registro y login se deben realizar utilizando **postman** .
 
Algunos ejemplo del cambio en las urls serían:

- **Registro de usuarios (POST)**:
 - En local: `http://loclahost:3000/apiv1/registro`
 - En el sevidor: **`http://begohorma.com/apiv1/registro`**
 
- **Login de usuario (POST)**:
	- En local: `http://loclahost:3000/apiv1/login`
	- En el servidor: **`http://begohorma.com/apiv1/login`**
	 
- **Conuslta básica de anuncios (GET)**:
	- En local: `http://localhost:3000/apiv1/anuncios?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`
	- En el servidor: **`http://begohorma.com/apiv1/anuncios?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`**


### Acceso a las imágenes estáticas desde el servidor
Los archivos estáticos en el servidor están servidos por nginx. Para acceder a las imágenes asociadas a los anunciós hay que realizar una petición **GET** a **`https://begohorma.com/images/anuncios/`** seguido del valor de **foto** del anuncio.

Algunos ejemplos son:

[https://begohorma.com/images/anuncios/bici.jpg](https://begohorma.com/images/anuncios/bici.jpg)

[http://begohorma.com/images/anuncios/iphone.png](http://begohorma.com/images/anuncios/iphone.png)

### Acceso a web estática a través de la IP pública del servidor

Accediendo a la **IP** publica del servidor: [35.163.248.119](http://35.163.248.119) se accede a una web estática, un blog de prueba basado en una plantilla de **https://startbootstrap.com** con algunas modificaciones en la cabeceras de las páginas.  


## Instalación en Local
* Una vez clonado el repositorio situarse en el directorio principal del API.

    `$ cd nodepop`
    
* Instalación de las dependencias necesarias

    `$ npm install`


### Instalación de base de datos de prueba
Debe tener arrancado mongoDB.

Ejecutar `$ npm run installDB`

Esto creará una colección de **anuncios** con  varios anuncios con datos de prueba y una colección **usuarios** con usuario para poder realizar pruebas.

Los datos del usuario de prueba son:

  - **nombre**: testUser
  
  - **email**: testemail@gmail.com
  
  - **clave**: 73s7Key
  

### Arranque de la aplicación
Una vez instalada una base de datos de pruebas y con mongoDB funcionando puede arrancar la API ejecutando:

  `npm start`


Para comprobar que la aplición se instalado correctamente desde un navegador acceder al la **url Base** :
  ** `http://localhost:3000/apiv1` **


Debería aparecer una web con el mensaje: **Nodepop** Welcome to Nodepop

### Calidad del código

Para garantizar la calidad del código se utiliza JSHint. Si se dispone del modulo JSHint instalado de forma global se puede pasar desede la línea de comandos desde el directorio prinicipal de la API 

`$ npm run jshint`

# Operaciones del API
El API realiza las siguientes operaciones:

- **Registro**: Para poder consultar los anuncios es necesario estar registrado

- **Autenticación**: Para acceder a los anuncios será necesario autenticar el usuario registrados mediante un token que se proporcionará al hacer login

- **Lista de anuncios Paginada**: Se podrán consultar los anuncios disponibles indicando una serie de condiciones y se obtendrá un JSON con los anuncios que las cumplan

- **Lista de tags existentes**: Se obtendrá un JSON con los tags disponibles. 

- **Acesso a imagenes estáticas**: Se podrá obtener la imagen asociada a un anuncio mediante una ruta fija de acceso a las imagenes más el nombre de la imagen asociada al anuncio.


Cada una de las operaciones se detallarán a continuación.

## Registro de usuarios
Para poder acceder a los anuncios será necesario disponer de un usuario registrado.

Para **registrar un nuevo usuario** hay que realizar una peticion **POST** a la url :  `http://loclahost:3000/apiv1/registro`
con los siguientes **parámetros** en el **body**:

  - **nombre**: nombre se usuario. Es obligatorio.
  
  - **email**: dirección de correo. Es obligatorio. Sólo puede existir un usuario con ese email.
  
  - **clave**: contraseña del usuario.
  
    - La clave se guardará en un hash


### Ejemplo de respuesta correcta:

```json
{
  "success": true,
  "usuario": {
    "nombre": "nombreUsuario1",
    "email": "testemail1@gmail.com",
    "clave": "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4"
  }
}
```

### Ejemplo de respuesta si no se indica el parámetro nombre

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "Param 'nombre' is required",
    "descripcion": "",
    "codigo": ""
  }
}
```

### Ejemplo de respuesta si no se indica el parámetro email

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "Param 'email' is required",
    "descripcion": "",
    "codigo": ""
  }
}
```

### Ejemplo de respuesta si no se indica el parámetro clave

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "Param 'clave' is required",
    "descripcion": "",
    "codigo": ""
  }
}
```

### Ejemplo de respuesta si se indica un email que ya existe

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "Email already exists. Only one user per email is permmitted",
    "descripcion": "",
    "codigo": ""
  }
}
```

## Autenticación

Una vez que se dispone de un usuario registrado, para poder acceder a los listados de anuncios y tags, será necesario hacer logín para obtener un token que será necesario incluir en cada una de las peticiones.

Para  hacer **login con un usuario registrado** hay que realizar una peticion **POST** a la url :  `http://loclahost:3000/apiv1/login`
con los siguientes **parámetros** en el **body**:

  - **nombre**: nombre se usuario. Es obligatorio.
  
  - **email**: dirección de correo. Es obligatorio. 
  
  - **clave**: contraseña del usuario.

Los errores recibidos si no se incluye alguno de los parámetros son los mismos que en el caso del registro.

Si los datos del usuario coinciden con los de un usuario registrado se devolverá un **token** válido durante *dos días* que se deberá incluir en las peticiones de tres posibles formas:

 - parámetro en el **body** llamado **token**
 
 - parámetro en la **query** llamado **token**
 
 - parámetro en el **header** llamado **x-access-token**
 
 

### Ejemplo de respuesta a solicitud de login correcta:

```json
{
  "sucess": true,
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc4MTgxNDMsImV4cCI6MTQ3Nzk5MDk0M30.W5lc7VyOyqdZEI8l4ZhJECnsFt4Kq4THNE8z32bzZ7E"
}
```

### Ejemplo de respuesta si los datos del usuario no están en la base de datos

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "User not found",
    "descripcion": "",
    "codigo": ""
  }
}
```

### Ejemplo de respuesta si la clave no coincide con los datos del usuario indicado

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "Pass is wrong",
    "descripcion": "",
    "codigo": ""
  }
}
```

##Lista de anuncios
La lista de anuncios requiere el token se autenticación indicado en la sección de **Autenticación**

Las peticiones deberán ser de tipo **GET** a la dirección: `http://localhost:3000/apiv1/anuncios?token=<UserToken> `

Al listado se le pueden aplicar filtros de búsquedas y modificadores para ordenar, seleccionar campos a mostrar,indicar el idioma del usuario, y obtener resultados paginados.

### Filtros
Los posibles campos por los que se pueden filtrar son:

 - **nombre**: devolverá los anuncios cuyo nombre de artículo empiece por el valor indicado en el parámetro
 
 - **venta**: especifica el tipo de anuncion. Si el valor del parámetro es *true* se mostrarán los anuncios de *venta* y si es *false* se mostrarán los anuncios de *búsqueda* de artículos.
 
 - **precio**: el valor del parámetro deberá seguir la siguiente estructura **"precion mínimo - precio máximo"** permitiendo varias combinaciones:
 
    - **precio mínimo - precio máximo** : especifica un rago de precios desde el mínimo al máximo ambos incluidos
    
    - **precio mínimo -** :especifica los precios *mayores o iguales* al precio mínimo indicado.
    
    - **- precio máximo** : especifica los precios *menores o iguales* al precio máximo indicado.
    
    - **precio** : especifica un precio *igual* al indicado

 - **tags** : devolverá los anuncios que contengan alguno de los tags especificados. Para solicitar varios tags hay que repetir el parameto *tag* por cada valor a budcar. Por ejemplo: `tag=valor1&tag=valor2&tag=valor3`
 

### Modificadores
Los posibles modificadores son:

- **lg**: para indicar el **idioma** de la consulta. En esta versión el API está disponible en inglés `en` y español `es`. La especificación de un idioma permitirá recibir los mensajes de error en el idioma indicado. Si no se especifica un idioma los errores se mostrarán en inglés. Una vez especificado el idioma en una consulta éste queda guardado.

- **sort**: permite devolver los anuncios ordenados por el campo especificado. Por defecto la ordenación es de forma *ascendente* si se quiere de forma *descendente* hay que indicar un **-** delante del campo de ordenación. Se pueden indicar varios campos de ordenación separados por un espacio.

- **fields**: permite seleccionar los campos de los anuncios que devolverá la busqueda. Para indicar varios campos deben ir separados por espacio. El campo **_id** se mostrará siempre.

Para obtener resultados paginados se pueden utilizar los siguientes modificadores:
- **start**: indica el número a partir del cual se comienzan a mostrar los resultados. Debe ser numérico

- **limit**: indica el número máximo de resultados que se devuelven. Debe ser numérico

- **includeTotal**: si es *true* mostrá el número total de anuncios que cumplen con los criterios se selección

A continuación se indican varios ejemplos de búsquedas correctas e incorrectas.



#### Ejemplo de listado anuncios correcto más simple - sin filtros

`http://localhost:3000/apiv1/anuncios?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "__v": 0,
      "nombre": "elit",
      "venta": false,
      "precio": 740.66,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ed",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 1543.89,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "work"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ee",
      "__v": 0,
      "nombre": "aliqua",
      "venta": true,
      "precio": 3626.5,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ef",
      "__v": 0,
      "nombre": "proident",
      "venta": false,
      "precio": 1386.48,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "__v": 0,
      "nombre": "minim",
      "venta": true,
      "precio": 965.19,
      "foto": "bici.jpg",
      "tags": [
        "mobile",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f1",
      "__v": 0,
      "nombre": "anim",
      "venta": false,
      "precio": 1585.78,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f2",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 3195.24,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    }
  ]
}
```

#### Ejemplo de listado filtrado por nombre

Anuncios cuyo nombre de artículo comienza por **b**

`http://localhost:3000/apiv1/anuncios?nombre=b&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    }
  ]
}
```

#### Ejemplos de listado filtrado por venta

Anuncios cuyo tipo es **venta**

`http://localhost:3000/apiv1/anuncios?venta=true&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ed",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 1543.89,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "work"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ee",
      "__v": 0,
      "nombre": "aliqua",
      "venta": true,
      "precio": 3626.5,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "__v": 0,
      "nombre": "minim",
      "venta": true,
      "precio": 965.19,
      "foto": "bici.jpg",
      "tags": [
        "mobile",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f2",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 3195.24,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "motor"
      ]
    }
  ]
}
```

Anuncios cuyo tipo es **búsqueda**

`http://localhost:3000/apiv1/anuncios?venta=false&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "__v": 0,
      "nombre": "elit",
      "venta": false,
      "precio": 740.66,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ef",
      "__v": 0,
      "nombre": "proident",
      "venta": false,
      "precio": 1386.48,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f1",
      "__v": 0,
      "nombre": "anim",
      "venta": false,
      "precio": 1585.78,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    }
  ]
}
```

#### Ejemplos de listado filtrados por precio

Anuncios cuyo precio está **entre 50 y 1000**

`http://localhost:3000/apiv1/anuncios?precio=50-1000&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "__v": 0,
      "nombre": "elit",
      "venta": false,
      "precio": 740.66,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "__v": 0,
      "nombre": "minim",
      "venta": true,
      "precio": 965.19,
      "foto": "bici.jpg",
      "tags": [
        "mobile",
        "motor"
      ]
    }
  ]
}
```



Anuncios cuyo precio es **mayor o igual a 1500**

`http://localhost:3000/apiv1/anuncios?precio=150-&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ed",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 1543.89,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "work"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f1",
      "__v": 0,
      "nombre": "anim",
      "venta": false,
      "precio": 1585.78,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f2",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 3195.24,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ee",
      "__v": 0,
      "nombre": "aliqua",
      "venta": true,
      "precio": 3626.5,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "motor"
      ]
    }
  ]
}
```

Anuncios cuyo precio es **menor o igual a 100**

`http://localhost:3000/apiv1/anuncios?precio=-100&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    }
  ]
}
```

Anuncios cuyo precio es **igual a 50**

`http://localhost:3000/apiv1/anuncios?precio=50&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    }
  ]
}
```

#### Ejemplos de listados filtrados por tags

Anuncios que contiene el tag **work**

`http://localhost:3000/apiv1/anuncios?tags=work&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ed",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 1543.89,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "work"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    }
  ]
}
```

Anuncios que contiene el tag **work** **o** el tag **lifestyle**

`http://localhost:3000/apiv1/anuncios?tags=work&tags=lifestyle&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "__v": 0,
      "nombre": "elit",
      "venta": false,
      "precio": 740.66,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ee",
      "__v": 0,
      "nombre": "aliqua",
      "venta": true,
      "precio": 3626.5,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ef",
      "__v": 0,
      "nombre": "proident",
      "venta": false,
      "precio": 1386.48,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ed",
      "__v": 0,
      "nombre": "tempor",
      "venta": true,
      "precio": 1543.89,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "work"
      ]
    }
  ]
}
```

#### Ejemplos de uso del modificador fields para seleccionar los campos de salida

Anuncios cuyo precio está **entre 50 y 1000** mostrando solo los campos **nombre** y **precio**

`http://localhost:3000/apiv1/anuncios?precio=50-1000&fields=nombre precio&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "nombre": "iPhone 3GS",
      "precio": 50
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "nombre": "voluptate",
      "precio": 50
    },
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "nombre": "Bicicleta",
      "precio": 230.15
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "nombre": "elit",
      "precio": 740.66
    },
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "nombre": "minim",
      "precio": 965.19
    }
  ]
}
```

Anuncios que contienen el **tag mobile** mostrando solo los campos **nombre**,**precio** y **tags**

`http://localhost:3000/apiv1/anuncios?tags=mobile&fields=nombre precio tags&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "nombre": "iPhone 3GS",
      "precio": 50,
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ef",
      "nombre": "proident",
      "precio": 1386.48,
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "nombre": "minim",
      "precio": 965.19,
      "tags": [
        "mobile",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f1",
      "nombre": "anim",
      "precio": 1585.78,
      "tags": [
        "motor",
        "mobile"
      ]
    }
  ]
}
```

#### Ejemplo de listados ordenados

Anuncios con **precio menor o igual a 50** y ordenado por **precio descendente** y **nombre ascendente** 

`http://localhost:3000/apiv1/anuncios?precio=-50&sort=-precio nombre&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    }
  ]
}
```

Anuncios con **precio menor o igual a 50** y ordenado por **precio ascendente** y **nombre descendente**

`http://localhost:3000/apiv1/anuncios?precio=-50&sort=precio -nombre&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "auncios": [
    {
      "_id": "5815d2f22ac0db6064de86eb",
      "__v": 0,
      "nombre": "bici Cross",
      "venta": true,
      "precio": 9,
      "foto": "bici.jpg",
      "tags": [
        "work",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    }
  ]
}
```

#### Ejemplos de listados paginados utilizando la combinacion de modificadores: start, limit, includeTotal

Anuncios que contienen los **tags mobile o lifestyle** **ordenados** por **precio** y mostrando como **máximo 4** empezando por el **primero** y mostrando el total de anuncios que cumplen la condición

`http://localhost:3000/apiv1/anuncios?tags=mobile&tags=lifestyle&sort=precio&start=0&limit=4&includeTotal=true&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "anuncios": [
    {
      "_id": "5815d2f22ac0db6064de86ea",
      "__v": 0,
      "nombre": "iPhone 3GS",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f3",
      "__v": 0,
      "nombre": "voluptate",
      "venta": false,
      "precio": 50,
      "foto": "iphone.png",
      "tags": [
        "work",
        "lifestyle"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86e9",
      "__v": 0,
      "nombre": "Bicicleta",
      "venta": true,
      "precio": 230.15,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ec",
      "__v": 0,
      "nombre": "elit",
      "venta": false,
      "precio": 740.66,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "lifestyle"
      ]
    }
  ],
  "totalResultados": 8
}
```

Anuncios que contienen los **tags mobile o lifestyle** **ordenados** por **precio** y mostrando como **máximo 4** empezando por el **cuarto** y mostrando el total de anuncios que cumplen la condición

`http://localhost:3000/apiv1/anuncios?tags=mobile&tags=lifestyle&sort=precio&start=5&limit=4&includeTotal=true&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": true,
  "anuncios": [
    {
      "_id": "5815d2f22ac0db6064de86f0",
      "__v": 0,
      "nombre": "minim",
      "venta": true,
      "precio": 965.19,
      "foto": "bici.jpg",
      "tags": [
        "mobile",
        "motor"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ef",
      "__v": 0,
      "nombre": "proident",
      "venta": false,
      "precio": 1386.48,
      "foto": "bici.jpg",
      "tags": [
        "lifestyle",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86f1",
      "__v": 0,
      "nombre": "anim",
      "venta": false,
      "precio": 1585.78,
      "foto": "iphone.png",
      "tags": [
        "motor",
        "mobile"
      ]
    },
    {
      "_id": "5815d2f22ac0db6064de86ee",
      "__v": 0,
      "nombre": "aliqua",
      "venta": true,
      "precio": 3626.5,
      "foto": "iphone.png",
      "tags": [
        "lifestyle",
        "motor"
      ]
    }
  ],
  "totalResultados": 8
}
```

#### Ejemplos de respuestas de error cuando se introduce algún parámetro incorrecto

Respuesta cuando se introduce un valor incorrecto de **venta**. Si no se ha indicado el **idioma** por **defecto** el mensaje de error se mostrará en **inglés**.

`http://localhost:3000/apiv1/anuncios?venta=abc&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "The 'venta' value is wrong. It must be 'true' or 'false'",
    "descripcion": "",
    "codigo": ""
  }
}
```

Si se indica en la petición el **idioma** utilzando el parámetro **lg** se puede cambiar el idioma de los errores a **español**

`http://localhost:3000/apiv1/anuncios?venta=abc&lg=es&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "El valor de 'venta' es incorrecto. Debe ser 'true' o 'false'",
    "descripcion": "",
    "codigo": ""
  }
}
```

Si se selecciona un idioma **distinto** de ingles 'en' o español 'es'

`http://localhost:3000/apiv1/anuncios?venta=abc&lg=fr&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc2ODA2NzIsImV4cCI6MTQ3Nzg1MzQ3Mn0.frnsOureP93a1msq466S2aWlA4P5yor9BH-wNolI3D4`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "El idioma seleccionado no está disponible. El idioma debe ser 'es' o 'en'",
    "descripcion": "",
    "codigo": ""
  }
}
```

Respuesta cuando se introduce un valor no numérico para el **precio**

`http://localhost:3000/apiv1/anuncios?venta=true&lg=es&precio=a34&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc4NTUxMzQsImV4cCI6MTQ3ODAyNzkzNH0.rT9EJLmqXHvkohZCbDuwurFc3j2eAaua57mldl1ef2Q`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "El valor de 'precio' es incorrecto. Debe ser un número",
    "descripcion": "",
    "codigo": ""
  }
}
```

Respuesta cuando se introduce **un valor de precio númerico y otro no**

`http://localhost:3000/apiv1/anuncios?venta=true&lg=es&precio=a34-1000&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc4NTUxMzQsImV4cCI6MTQ3ODAyNzkzNH0.rT9EJLmqXHvkohZCbDuwurFc3j2eAaua57mldl1ef2Q`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "El valor de 'precio' es incorrecto. El precio mínimo y el  precio máximo deben ser números",
    "descripcion": "",
    "codigo": ""
  }
}
```

Respuesta cuando el valor de **includeTotal** no es un valor boolano

`http://localhost:3000/apiv1/anuncios?&includeTotal=a& venta=true&precio-50&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc4NTUxMzQsImV4cCI6MTQ3ODAyNzkzNH0.rT9EJLmqXHvkohZCbDuwurFc3j2eAaua57mldl1ef2Q`

```json
{
  "success": false,
  "error": {
    "name": "Nodepop 1.0  Error",
    "mensaje": "The 'includeTotal' value is wrong. It must be 'true' or 'false'",
    "descripcion": "",
    "codigo": ""
  }
}
```

## Lista de tags existentes
Para obtener los tags disponibles hay que realizar una petición de tipo **GET** a `http://localhost:3000/apiv1/tags` e incluir el token que autentica al usuario.

La respuesta será un JSON con un array denominado **tags** que contiene los posible valores de los tags

`http://localhost:3000/apiv1/tags?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0Nzc4NTUxMzQsImV4cCI6MTQ3ODAyNzkzNH0.rT9EJLmqXHvkohZCbDuwurFc3j2eAaua57mldl1ef2Q`

```json
{
  "success": true,
  "tags": [
    "work",
    "lifestyle",
    "motor",
    "mobile"
  ]
}
```

## Acceso a imagenes estáticas

Para acceder a las imagenes asociadas a los anuncios hay que realizar una peticion de tipo **GET** a `http://localhost:3000/images/anuncios/` seguido del valor de **foto** del anuncio

Algunos ejemplo son:

`http://localhost:3000/images/anuncios/iphone.png`

`http://localhost:3000/images/anuncios/bici.jpg`