/**
 * Created by begohorma on 22/10/16.
 */
'use strict';


console.log('*********INSTALLING DB *****************');

require('./mongoConnection');
require('../models/Anuncio');
require('../models/Usuario');

//configuración local
var localConfig =require('../localConfig');

//CustomError
var CustomError=require('../routes/apiv1/CustomError');

//hash para la clave
var hash=require('hash.js');


var mongoose= require('mongoose');

//Fichero json con los anuncios a cargar
var anunciosJson= require('./anuncios.json');

// //Acceso a los modelos (schemas)
var Anuncio = mongoose.model('Anuncio');
var Usuario = mongoose.model('Usuario');

//TODO - Cambiarlo por promesas

//Borrar todos los anuncios
Anuncio.deleteAll(function (err) {
    if(err){
        console.log('error borrando anuncios');
        return ( new CustomError('DELETING_ADVERTISEMENTS',err));
    }
    console.log('Coleccion anuncios borrada');

    //Borrar todos los usuarios
    Usuario.deleteAll(function (err) {
        if(err){
            console.log('error borrando usuarios');
            return ( new CustomError('DELETING_USERS',err));
        }
        console.log('Coleccion usuarios borrada');

        //Insertar los anuncios del fichero json

        Anuncio.insertMany(anunciosJson.anuncios,function(err,anunciosInsertados){
            if(err){
                console.log('Error insertando anucios');
                return ( new CustomError('ADDING_ADVERTISEMENTS',err));
            }
            console.log('Se han insertado los anuncios: ', anunciosInsertados);
            
            //Insertar usuario de prueba
            
            let clavehash=hash.sha256().update(localConfig.testUser.clave).digest('hex');
            const usuarioPruebas = new Usuario({nombre: localConfig.testUser.name ,email:localConfig.testUser.email , clave: clavehash});

            usuarioPruebas.save(function (err, usuarioCreado) {
                if(err){
                    console.log('Error creando el usario de pruebas');
                    return ( new CustomError('ADDING_TEST_USER',err));
                }
                console.log('Se ha creado el usuario de pruebas:', usuarioCreado);

            });// + Usuario

        });//+ Anuncios
    }); //-  Usuario
}); //- Anuncios










