/**
 * Created by begohorma on 25/10/16.
 */
'use strict';

var literales = require('./errors.json');
var errors= literales.errors;

//configuración local
var localConfig =require('../localConfig');

//TODO ¿Asíncorna?
var traducir = function(clave) {
try{
        let mensaje = null;
        errors.forEach(function (error) {

            if (clave === error.clave) {
                mensaje = localConfig.language.userLanguage === 'es' ? error.es : error.en;
            }
        });
        return mensaje;
}
catch(exception){
    console.log('Exception:',exception.message);
}

};

module.exports= traducir;