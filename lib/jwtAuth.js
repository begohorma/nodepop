/**
 * Created by begohorma on 26/10/16.
 */
'use strict';

var jwt= require('jsonwebtoken');

//configuración local
var localConfig =require('../localConfig');

const secret=localConfig.jwt.secret;
//CustomError
var CustomError=require('../routes/apiv1/CustomError');

module.exports = function() {

    return function(req, res, next) {

        //obtener el token de cuerpo, los parámetros de la query o de la cabecera
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decodificar el token
        if (token) {

            //verificar secret
            jwt.verify(token, secret, function(err, decoded) {
                if (err) {
                    return res.json(
                        {
                            ok: false,
                             error: new CustomError('FAILED_AUTH_TOKEN','',401)
                         });
                    //error: {code: 401, message: 'Failed to authenticate token.'}});
                } else {
                    //Si es correcto pasar al siguiente middleware
                    req.decoded = decoded;
                    console.log('decoded', decoded);
                    next();
                }
            });

        } else {

            //Si no hay token mostrar un error
            return res.status(403).json({
                ok: false,
                //error: { code: 403, message: 'No token provided.'}
                error:new CustomError('NO_TOKEN_PROVIDED','',403)
            });

        }
    };
};